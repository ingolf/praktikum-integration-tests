# Интеграционные тесты для Я.Практикум
## Установка
1. Для начала ставим selenium-standalone по [этой инструкции](https://github.com/gemini-testing/hermione#prerequisites).
Не забываем поставить JDK.
1. Устанавливаем Yandex Browser как основной для тестирования
1. yarn install 

## Варианты запуска
1. yarn test
