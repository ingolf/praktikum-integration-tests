/**
 * @param {Object} elem ожидаемый элемент
 * @param {Boolean} reverse если true, то ждёт обратного
 */
async function waitExist(elem, reverse) {
    await this.waitForExist(elem, 10000, reverse);
}

/**
 * @param {Object} elem ожидаемый элемент
 * @param {Boolean} reverse если true, то ждёт обратного
 */
async function waitEnabled(elem, reverse) {
    await this.waitForEnabled(elem, 10000, reverse);
}

/**
 * @param {Object} elem ожидаемый элемент
 * @param {Boolean} reverse если true, то ждёт обратного
 */
async function waitVisible(elem, reverse) {
    await this.waitForVisible(elem, 10000, reverse);
}

module.exports = {
    waitExist,
    waitEnabled,
    waitVisible
};
