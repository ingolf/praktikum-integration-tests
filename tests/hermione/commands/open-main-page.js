const commonPO = require('../page-objects/landings/common');

// открыть гравную страницу
async function openMainPage() {
    await this.url('/');
    await Promise.all([
        this.waitExist(commonPO.page.header()),
        this.waitExist(commonPO.page.content())
    ]);
}

module.exports = {
    openMainPage
};
