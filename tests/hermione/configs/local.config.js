const os = require('os');

let chromiumBinary;
let yaBroBinary;

if (os.platform() === 'darwin') {
    yaBroBinary = '/Applications/Yandex.app/Contents/MacOS/Yandex';
    chromiumBinary = '/Applications/Chromium.app/Contents/MacOS/Chromium';
} else {
    yaBroBinary = '/usr/bin/yandex-browser';
    chromiumBinary = '/usr/bin/chromium-browser';
}

module.exports = {
    gridUrl: 'http://localhost:4444/wd/hub',
    baseUrl: 'https://praktikum.yandex.ru',//'https://localhost.msup.yandex.ru:8000',
    browsers: [
        'YandexBrowser'
        //'Chromium'
    ],
    browsersDescription: {
        YandexBrowser: {
            desiredCapabilities: {
                browserName: 'chrome',
                chromeOptions: {
                    binary: yaBroBinary
                }
            }
        },
        Chromium: {
            desiredCapabilities: {
                browserName: 'chrome',
                chromeOptions: {
                    binary: chromiumBinary
                }
            }
        }
    }
};
