const pageObject = require('bem-page-object');
const PO = {};
const Entity = pageObject.Entity;

// общий блок с содержимым страниц
PO.page = new Entity('.page');

// шапка
PO.page.header = new Entity('.header');

PO.page.content = new Entity({block: 'page', elem: 'content'});

module.exports = pageObject.create(PO);