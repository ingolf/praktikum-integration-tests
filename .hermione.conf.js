const requireDir = require('require-dir');
const config = require('./tests/hermione/configs/local.config');

module.exports = {
    gridUrl: config.gridUrl,
    baseUrl: config.baseUrl,
    waitTimeout: 15000,
    retry: 1,
    system: {
        debug: false,
        mochaOpts: {
            timeout: 5 * 60 * 1000 // 5m test-case timeout
        }
    },
    sets: {
        desktop: {
            files: 'tests/hermione/desktop/**/*.test.js',
            browsers: config.browsers
        }
    },
    browsers: config.browsersDescription,
    prepareBrowser: browser => {
        let commands = requireDir('./tests/hermione/commands');

        for (const helpers of Object.values(commands)) {
            for (let commandName of Object.keys(helpers)) {
                browser.addCommand(commandName, helpers[commandName]);
            }
        }
    }
};
