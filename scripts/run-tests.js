const {exec} = require('child_process');

const child = exec(`node_modules/.bin/hermione`);

child.stdout.setEncoding('utf8');

// eslint-disable-next-line no-console
child.stdout.on('data', console.log);

child.on('close', (code) => {
    // eslint-disable-next-line no-console
    console.log(`child process(run-tests) exited with code ${code}`);
});
